# Deployed Mosquitto message broker with ConfigMap and Secret Volume types 

## Table of Contents

- [Project Description](#project-description)

- [Technologies Used](#technologies-used)

- [Steps](#steps)

- [Installation](#installation)

- [Screenshots](#screenshots)

- [Usage](#usage)

- [How to Contribute](#how-to-contribute)

- [Questions](#questions)

- [References](#references)

- [License](#license)

## Project Description 

Defined configuration and passwords for Mosquitto message broker with ConfigMap and Secret Volume types 

## Technologies Used 

* Kubernetes 

* Docker 

* Mosquitto 

## Steps 

Step 1: Create a configMap  for mosquitto 

    kubectl apply -f config-file.yaml 

[ConfigMap Created](/images/01_create_a_config_map_file_for_mosquitto.png)

Step 2: Create a secret for mosquitto 

    kubectl apply -f secret-file.yaml 

[Secret Created](/images/02_secret_file_created.png)

Step 3: Mount local volumes secret and configmap into the mosquitto container in Mosquitto Deployment config file 

[Mounting volume into pod then container](/images/03_mounting_volume_into_pod_then_container.png)

Step 4: Apply Mosquitto deployment 

    kubectl apply -f mosquitto.yaml 

Step 5: Check if volumes are mounted inside the container through interactive terminal 

    kubectl exec -it mosquitto-6dc97f4c59-2cpdc -- /bin/sh 

[Volumes in container](/images/05_checking_if_volumes_are_mounted_inside_the_container_through_interactive_mode.png)

## Installation

Run $ brew install minikube 

## Usage 

Run $ kubectl apply -f mosquitto.yaml

## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/k8-deploy-mosquito-message-broker.git

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review



## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

https://gitlab.com/omacodes98/k8-deploy-mosquito-message-broker

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.